<!DOCTYPE HTML>
<html>
<head>
<style>
.logo{
	width:60%;
}

</style>
<link rel="stylesheet" href="css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
<link rel="stylesheet" type="text/css" href="css/form.css">
</head>
<body>
<nav class="navbar navbar-success">
  <div class="container-fluid">
    <div class="navbar-header">
      <a class="navbar-brand" href="#">
        <img alt="Brand" src="img/logo.png" class="logo">
      </a>
    </div>
  </div>
</nav>
<div class="container" width:50% >
<div class="panel panel-success">
	<div class="panel-body">
		<form class="form-horizontal" action="form.php" method="post">
		  <div class="form-group">
			<label for="name" class="col-sm-2 control-label">NAME</label>
			<div class="col-xs-12 col-sm-6 col-md-8">
			  <input type="text" class="form-control" id="name" name="name">
			</div>
		  </div>
		  <div class="form-group">
			<label for="contactNum" class="col-sm-2 control-label">PHONE NO.</label>
			<div class="col-xs-12 col-sm-6 col-md-8">
				<input type="text" class="form-control" id="number">
			</div>
		  </div>
		  <div class="form-group">
			<label for="email" class="col-sm-2 control-label">EMAIL ADD</label>
			<div class="col-xs-12 col-sm-6 col-md-8">
				<input type="email" class="form-control" id="email">
			</div>
		  </div>
		  <div class="form-group">
			<label for="school" class="col-sm-2 control-label">SCHOOL</label>
			<div class="col-xs-12 col-sm-6 col-md-8">
				<input type="text" class="form-control" id="school">
			</div>
		  </div>
		  <div class="form-group">
			<label for="question" class="col-sm-2 control-label">SCIENCE QUESTION</label>
			<div class="col-xs-12 col-sm-6 col-md-8">
			 <textarea class="form-control" id="question" rows="3" placeholder="Type your question here."></textarea>
			</div>
		  </div>

		  <div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
			  <button type="submit" class="btn btn-default btn-lg">SUBMIT</button>
			</div>
		  </div>
		</form>
	</div>
</div>
<div class="panel panel-success"name="info">
	<div class="panel-body">
		<h5 align="center"> Your questions will be answered during the Science Demo at <br>
			<br>11:00 AM
			<br>2:00 PM
			<br>5:00 PM<br>
			<br>The Science Demo will be at the Atom Gallery

		</h5>
	</div>
</div>
</div>
</body>
</html>
