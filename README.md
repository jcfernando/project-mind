# README #

### This is the repository for Project Mind by ACSS ###

* Ask-Answer system to be used in the Mind Museum in Taguig

### Dependencies ###
* MySQL, PHP, Bootstrap, NODEjs
* WAMP as the web server of PHP

### SPECIFICATION ###

* First Part
  1. Widget Part
    *  2/3 of the page contains data directly from the database; shows video, photo banner, etc.
    *  Focus: Creative UI and child-friendly design
    * > Technologies Required: MySql, PHP, bootstrap

  2. Answered Questions part
    * 1/3 of the page shows the answered questions approved by the admin
    * Focus: Free flowing, transition
    * > Technologies Required: MySql, PHP, Boostrap

* Second Part
   1. Form
    * Mobile App that gets the question by the user.
    * Form to be answered is in a tablet
    * > Technologies: PHP, MySql, bootstrap

* Third Part
   1. Admin SIDE
    * Contains all of the questions answered and unanswered by the admin
    * Shows all the question as a div that can be DELETED, ANSWERED or EDITED (if answered)
    * Can be classified as "Answered" or "Unanswered" OR "Shown to the screen" or "Not shown"
    * Can select many <div> of questions sections
    * > Technologies: MySql, Bootstrap, PHP/NodeJS (Discuss with group)